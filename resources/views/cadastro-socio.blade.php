@extends('layouts.default')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            Cadastrar Socio
        </div>
        <h3 style="color:black;">
                Digite o nome do sócio, escolha o clube e clique em cadastrar para concluir o cadastro:
        </h3>
        <form role="form" method="post" action="{{url('/cadastro-socio')}}">
            @csrf
            <div class="form-group">
                <input type="text" name="nomeSocio" id="nomeSocio" placeholder="Nome do socio...">
                <select name="idClube" id="clube">
                    @foreach ($listaClubes as $item)
                        @if (!empty($item->nome))
                            <option value="{{$item->id_clube_futebol}}">{{$item->nome}}</option>
                        @endif
                    @endforeach
                </select>
                <input type="submit" value="Cadastrar">
            </div>
        </form>
    </div>

@endsection