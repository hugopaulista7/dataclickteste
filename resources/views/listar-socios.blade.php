@extends('layouts.default')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            Sócios Cadastrados
        </div>
        <form action="/pesquisar-socios" method="POST" role="search">
            {{ csrf_field() }}
            <div class="input-group">
                <input type="text" class="form-control" name="socio"
                    placeholder="Procurar sócios"> <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                        Procurar
                    </button>
                </span>
            </div>
        </form>
        <div class="lista">
            @foreach ($listaSocios as $item)

            <table style="width: 100%">
                <tr>
                    <th>
                        <div class="item-lista">
                            <table>
                                <tr>
                                    <th>
                                        {{ $item->nomeSocio }} ||
                                    </th>
                                    <th>
                                        <p style="color:blue;">{{ $item->nome }}</p>
                                    </th>
                                </tr>
                            </table>
                        </div>
                        
                    </th>
                    <th>
                    <form action="{{url('/excluir-socio/'.$item->id_socio)}}">
                            <input type="submit" class="btn btn-xs pull-right" value="Excluir">
                        </form> 
                    </th>

                </tr>
            </table>
            @endforeach
        </div>
    </div>
@endsection