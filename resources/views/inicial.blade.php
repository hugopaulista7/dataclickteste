@extends('layouts.default')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            Teste Data Click
        </div>
    <form action="{{url('/cadastro-clube')}}">
        <input type="submit" value="Cadastrar Clubes">
    </form>
    <br>
    <form action="{{url('/listar-clubes')}}">
        <input type="submit" value="Listar Clubes Cadastrados">
    </form>
    <br>
    <form action="{{url('/cadastro-socio')}}">
        <input type="submit" value="Cadastrar Sócio">
    </form>
    <br>
    <form action="{{url('/listar-socios')}}">
        <input type="submit" value="Listar Sócios">
    </form>
    </div>    
@endsection