@extends('layouts.default')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            Clubes Cadastrados
        </div>
        <form action="/pesquisar-clubes" method="POST" role="search">
            {{ csrf_field() }}
            <div class="input-group">
                <input type="text" class="form-control" name="clube"
                    placeholder="Procurar clubes"> <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                        Procurar
                    </button>
                </span>
            </div>
        </form>
        <div class="lista">
            @foreach ($listaClubes as $item)

            <table style="width: 100%">
                <tr>
                    <th>
                        <div class="item-lista">
                            {{ $item->nome }}
                        </div>
                    </th>
                    <th>
                    <form action="{{url('/excluir-clube/'.$item->id_clube_futebol)}}">
                            <input type="submit" class="btn btn-xs pull-right" value="Excluir">
                        </form> 
                    </th>

                </tr>
            </table>
            @endforeach
        </div>
    </div>
@endsection