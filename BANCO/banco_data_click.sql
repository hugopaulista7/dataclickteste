-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14-Fev-2019 às 01:14
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teste_dataclick`
--
CREATE DATABASE IF NOT EXISTS `teste_dataclick` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `teste_dataclick`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clube_futebol`
--

CREATE TABLE `clube_futebol` (
  `id_clube_futebol` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `dt_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_core_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `clube_futebol`
--

INSERT INTO `clube_futebol` (`id_clube_futebol`, `nome`, `dt_cadastro`, `dt_alteracao`, `id_core_status`) VALUES
(1, 'sao paulo', '2019-02-13 17:05:22', '2019-02-13 20:53:50', 1),
(2, 'palmeiras', '2019-02-13 17:44:47', '2019-02-13 20:52:38', 1),
(3, 'cruzeiro', '2019-02-13 18:49:18', '2019-02-13 20:53:53', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `core_status`
--

CREATE TABLE `core_status` (
  `id_core_status` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `core_status`
--

INSERT INTO `core_status` (`id_core_status`, `nome`) VALUES
(1, 'Ativo'),
(2, 'Excluido');

-- --------------------------------------------------------

--
-- Estrutura da tabela `socio`
--

CREATE TABLE `socio` (
  `id_socio` int(11) NOT NULL,
  `nomeSocio` varchar(250) NOT NULL,
  `id_clube` int(11) NOT NULL,
  `dt_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_core_status_socio` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `socio`
--

INSERT INTO `socio` (`id_socio`, `nomeSocio`, `id_clube`, `dt_cadastro`, `dt_alteracao`, `id_core_status_socio`) VALUES
(1, 'Hugo', 1, '2019-02-13 20:06:43', '2019-02-13 22:16:06', 1),
(2, 'João', 2, '2019-02-13 20:06:58', '2019-02-13 22:06:58', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `socio_clube`
--

CREATE TABLE `socio_clube` (
  `id_socio_clube` int(11) NOT NULL,
  `id_socio` int(11) NOT NULL,
  `id_clube` int(11) NOT NULL,
  `id_core_status_socio_clube` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clube_futebol`
--
ALTER TABLE `clube_futebol`
  ADD PRIMARY KEY (`id_clube_futebol`);

--
-- Indexes for table `core_status`
--
ALTER TABLE `core_status`
  ADD PRIMARY KEY (`id_core_status`);

--
-- Indexes for table `socio`
--
ALTER TABLE `socio`
  ADD PRIMARY KEY (`id_socio`);

--
-- Indexes for table `socio_clube`
--
ALTER TABLE `socio_clube`
  ADD PRIMARY KEY (`id_socio_clube`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clube_futebol`
--
ALTER TABLE `clube_futebol`
  MODIFY `id_clube_futebol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `core_status`
--
ALTER TABLE `core_status`
  MODIFY `id_core_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `socio`
--
ALTER TABLE `socio`
  MODIFY `id_socio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `socio_clube`
--
ALTER TABLE `socio_clube`
  MODIFY `id_socio_clube` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
