<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clube extends Model {

    protected $table = 'clube_futebol';
    public $timestamps = false;
}
