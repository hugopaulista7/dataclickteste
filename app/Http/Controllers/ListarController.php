<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Clube;


class ListarController extends BaseController {
    const ID_ATIVO = 1;
    const ID_EXCLUIDO = 2;
    public function ListarClubes(Request $request) {
        $listaClubes = [];
        $listagem = $request->input('clube');
        if ($listagem != NULL) {

            $listaClubes = DB::table('clube_futebol')->where( [['nome', 'LIKE','%'.$listagem.'%'],[ 'id_core_status', '=', self::ID_ATIVO]])->paginate(10);
        }
        else {
            $listaClubes = DB::table('clube_futebol')->where('id_core_status', '=', self::ID_ATIVO)->get();
        }
        
        return view('/listar-clubes', ['listaClubes' => $listaClubes]);
        

    }

    public function ListarSocios(Request $request) {
        $listaSocios = [];
        $listagem = $request->input('socio');

        if ($listagem != NULL) {
            $listaSocios = DB::table('socio')->join('clube_futebol', 'socio.id_clube', '=', 'clube_futebol.id_clube_futebol')
            ->where([['nomeSocio', 'LIKE','%'.$listagem.'%'], ['id_core_status_socio', '=', self::ID_ATIVO]])
            ->select('socio.*', 'clube_futebol.nome')
            ->get();
            
        }
        else {
            $listaSocios = DB::table('socio')->join('clube_futebol', 'socio.id_clube', '=', 'clube_futebol.id_clube_futebol')
            ->where('id_core_status_socio', '=', self::ID_ATIVO)
            ->select('socio.*', 'clube_futebol.nome')
            ->get();
        }
        
        return view('/listar-socios', ['listaSocios' => $listaSocios]);
        

    }

    public function ExcluirClube($idClube) {
        DB::table('clube_futebol')->where('id_clube_futebol', $idClube)->update(['id_core_status' => self::ID_EXCLUIDO]);

        echo "Clube excluído com sucesso";
    }
    public function ExcluirSocio($idSocio) {
        DB::table('socio')->where('id_socio', $idSocio)->update(['id_core_status' => self::ID_EXCLUIDO]);

        echo "Sócio excluído com sucesso";
    }
}