<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

use App\Clube;
use App\Socio;

class CadastroController extends BaseController {

    public function CadastroClube(Request $request) {
        $nome = $request->input('nomeClube');
        try {
            $this->Cadastrar($nome, "Clube");
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }
    public function CadastroSocio(Request $request) {
        $dados = [];
        $dados['nomeSocio'] = $request->input('nomeSocio');
        $dados['idClube'] = $request->input('idClube');
        try {
            $this->Cadastrar($dados, "Socio");
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    private function Cadastrar($dados, $tipoCadastro) {
        if (empty($dados)) {
            throw new Exception("Dados não recebidos para o cadastro");
        }

        if (empty($tipoCadastro)) {
            throw new Exception("Tipo de cadastro não recebido");
        }
        $retorno = false;
        switch ($tipoCadastro) {
            case "Clube":
            $retorno = $this->CadastrarClube($dados);
            break;
            
            case "Socio":
            $retorno = $this->CadastrarSocio($dados);
            break;

            default:
            throw new Exception("Não existe esse tipo de cadastro");
            break;
        }

        return $retorno;
    }

    private function CadastrarSocio($dados) {
        if (empty($dados['nomeSocio'])) {
            throw new Exception("Nome não recebido");
        }

        if (empty($dados['idClube'])) {
            throw new Exception("Id do clube não recebido");
        }

        $socio = new Socio;
        $socio->nome = $dados['nomeSocio'];
        $socio->id_clube = $dados['idClube'];
        $socio->save();

        echo "Socio cadastrado com sucesso";
    }

    private function CadastrarClube($dados) {
        $clube = new Clube;
        $clube->nome = $dados;
        $clube->save();

        echo "Clube cadastrado com sucesso";

    }

    public function AbrirCadastroSocio() {
        $listaClubes = DB::table('clube_futebol')->where('id_core_status', '=', 1)->get();

        if (empty($listaClubes)) {
            $listaClubes = [
                ['nome' => 'Sem clube']  
            ];
        }

        return view('/cadastro-socio', ['listaClubes' => $listaClubes]);
    }
}