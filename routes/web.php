<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicial');
});

Route::get('/cadastro-clube', function () {
    return view('cadastro-clube');
});

Route::get('/cadastro-socio', 'CadastroController@AbrirCadastroSocio');

Route::get('listar-clubes', 'ListarController@ListarClubes');

Route::get('listar-socios', 'ListarController@ListarSocios');

Route::post('/cadastro-clube','CadastroController@CadastroClube');

Route::post('/cadastro-socio','CadastroController@CadastroSocio');

Route::get('/excluir-clube/{idClube}', 'ListarController@ExcluirClube');

Route::get('/excluir-socio/{idSocio}', 'ListarController@ExcluirSocio');

Route::any('/pesquisar-clubes', 'ListarController@ListarClubes');

Route::any('/pesquisar-socios', 'ListarController@ListarSocios');